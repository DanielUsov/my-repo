package PolygonsText;

class Text implements MyInterface{
   String text;

   public Text(String text) {
      this.text = text;
   }

   public String output() {
      return this.text;
   }
}
