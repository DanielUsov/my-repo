package PolygonsText;

class MyException extends Exception{
   public String message;

   public MyException(String message){
      super(message);
   }

   public String getMessage() {
      return message;
   }
}
