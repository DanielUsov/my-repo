package PolygonsText;

import java.util.Arrays;

class Triangle extends Polygons{
   public Triangle(int[] arcs)throws MyException{
      super(arcs);
      if (arcs.length != 3) {
         throw new MyException("В треугольнике только три стороны");
      }
   }

   public String output(){
      if (getPerimeter() <= 0) {
         return "Этот не геометрическая фигура";
      }
      else {
         return "Дуги триугольника: " + Arrays.toString(getArcs()) + " , периметр равен: " + getPerimeter();
      }
   }
}
