package PolygonsText;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestPolygon{
   @Test void TestPerimeter() throws MyException {
      Polygons a = new Polygons(new int[]{1, 2, 3, 4, 5});
      assertEquals(15, a.getPerimeter());
   }
}
