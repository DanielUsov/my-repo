package PolygonsText;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestTriangle{
   @Test void TestPerimeter() throws MyException {
      Triangle a = new Triangle(new int[]{1, 2, 3});
      assertEquals(6, a.getPerimeter());
   }
}
