package Operations2DMatrices;

class MatrixException extends Exception{
   String message;

   MatrixException(String message){
      this.message = message;
   }

   public String getMessage(){
      return this.message;
   }

   public void setMessage(String message){
      this.message = message;
   }
}
