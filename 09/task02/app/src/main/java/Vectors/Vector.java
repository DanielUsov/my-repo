package Vectors;

public class Vector{
   private double x;
   private double y;

   public Vector(double x, double y){
      this.x = x;
      this.y = y;
   }

   public double getX(){
      return this.x;
   }

   public void setX(double x){
      this.x = x;
   }

   public double getY(){
      return this.y;
   }

   public void setY(double y){
      this.y = y;
   }

   public Vector sum(Vector secondVector){
      this.x += secondVector.getX();
      this.y += secondVector.getY();
      return this;
   }

   public Vector sub(Vector secondVector){
      this.x -= secondVector.getX();
      this.y -= secondVector.getY();
      return this;
   }

   public Vector mul(double scalar){
      this.x *= scalar;
      this.y *= scalar;
      return this;
   }

   public Vector div(double scalar){
      this.x /= scalar;
      this.y /= scalar;
      return this;
   }

   @Override
   public String toString(){
      return "(" + this.x + ";" + this.y + ")";
   }

   @Override
   public boolean equals(Object o){
      if (this.getClass() == o.getClass()) {
         Vector vector = (Vector) o;
         if (Double.compare(vector.x, x) == 0 && Double.compare(vector.y, y) == 0) {
            return true;
         }
         else {
            return false;
         }
      }
      else {
         return false;
      }
   }
}
