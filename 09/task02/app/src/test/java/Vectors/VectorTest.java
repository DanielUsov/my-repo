package Vectors;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class VectorTest{
   @Test void getX(){
      double x = 10;
      Vector testVector = new Vector(x, 10);
      assertEquals(x, testVector.getX());
   }

   @Test void getY(){
      double y = 10;
      Vector testVector = new Vector(10, y);
      assertEquals(y, testVector.getY());
   }

   @Test void sum() {
      Vector testVector1 = new Vector(10, 10);
      Vector testVector2 = new Vector(20, 20);
      Vector answer = new Vector(30, 30);
      assertEquals(answer, testVector1.sum(testVector2));
   }

   @Test void sub() {
      Vector testVector1 = new Vector(10, 10);
      Vector testVector2 = new Vector(20, 20);
      Vector answer = new Vector(-10, -10);
      assertEquals(answer, testVector1.sub(testVector2));
   }

   @Test void mul() {
      Vector testVector = new Vector(10, 10);
      Vector answer = new Vector(20, 20);
      assertEquals(answer, testVector.mul(2));
   }

   @Test void div() {
      Vector testVector = new Vector(10, 10);
      Vector answer = new Vector(5, 5);
      assertEquals(answer, testVector.div(2));
   }
}
