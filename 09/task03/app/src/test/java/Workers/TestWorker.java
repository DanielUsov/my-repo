package Workers;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestWorker{
   MyWorkers worker = new Worker("Олег", "Разнорабочий", 25000);

   @Test void workerName(){
      assertEquals("Олег", worker.getName());
   }
   @Test void workerPosition(){
      assertEquals("Разнорабочий", worker.getPosition());
   }
   @Test void workerSalary(){
      assertEquals(25000, worker.getSalary());
   }
   @Test void workerMyWork(){
      assertEquals("Простая работа", ((Worker)worker).myWork());
   }

}
