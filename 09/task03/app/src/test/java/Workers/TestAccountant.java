package Workers;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestAccountant{
   MyWorkers accountant = new Accountant("Лариса", "Младший бухгалтер", 45000);

   @Test void accountantName(){
      assertEquals("Лариса", accountant.getName());
   }
   @Test void accountantPosition(){
      assertEquals("Младший бухгалтер", accountant.getPosition());
   }
   @Test void accountantSalary(){
      assertEquals(45000, accountant.getSalary());
   }
   @Test void accountantMyWork(){
      assertEquals("Работаю с финансами", ((Accountant)accountant).myWork());
   }

}
