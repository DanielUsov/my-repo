package Workers;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestChiefAccountant{
   MyWorkers chiefAccountant = new ChiefAccountant("Глеб", "Главный бухгалтер", 50000);

   @Test void chiefAccountantName(){
      assertEquals("Глеб", chiefAccountant.getName());
   }
   @Test void chiefAccountantPosition(){
      assertEquals("Главный бухгалтер", chiefAccountant.getPosition());
   }
   @Test void chiefAccountantSalary(){
      assertEquals(50000, chiefAccountant.getSalary());
   }
   @Test void chiefAccountantMyWork(){
      assertEquals("Работаю с большими финансами", ((ChiefAccountant)chiefAccountant).myWork());
   }

}
