package Workers;

/*
Создать классы для описания работников: работник в общем, бухгалтер, главный бухгалтер,
инженер, рабочий. Все имеют имя, должность и зарплату. Добавить характерные для каждого класса в
отдельности методы. Переопределить все доступные методы класса Object адекватным образом.
Создать массив, содержащий объекты всех классов описания работников, и вывести его на консоль.
*/

class MyWorkers{
   public String name;
   public String position;
   public int salary;

   public String getName(){
      return name;
   }

   public void setName(String name){
      this.name = name;
   }

   public String getPosition(){
      return position;
   }

   public void setPosition(String position){
      this.position = position;
   }

   public int getSalary(){
      return salary;
   }

   public void setSalary(int salary){
      this.salary = salary;
   }

   @Override
   public String toString() {
      return "Имя: " + getName() + "\n" +
            "Должность: " + getPosition() + "\n" +
            "ЗП: " + getSalary() + "\n";
   }

   @Override
   public boolean equals(Object obj) {
      if (obj instanceof MyWorkers) {
           if (this.name == ((MyWorkers) obj).getName() &&
               this.position == ((MyWorkers) obj).getPosition() &&
               this.salary == ((MyWorkers) obj).getSalary()) {
               return true;
           }
      }
      return false;
   }

   @Override
   public int hashCode() {
      int hash = name.length() * 100 + position.length() * 100 + salary;
      return hash;
   }
}
