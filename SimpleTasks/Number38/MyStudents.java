public class MyStudents {
   private String name;
   private String group;

   public MyStudents(String name, String group) {
      this.name = name;
      this.group = group;
   }

   public String getName() {
      return this.name;
   }

   public String getStudents() {
      return "Имя: " + this.name + "\n" + "Группа: " + this.group;
   }

   public String getGroup() {
      return this.group;
   }

   public void setGroup(String group) {
      this.group = group;
   }
}
