package Number29;
//дана строка из букв и пробелов, найти количество слов в ней.

class MyProgram{
  public static void main(String[] args) {
    String str = " a b  c";
    int counterLength = 0;
    for (int i = 0;i < str.length() ;i++ ) {
      if (str.charAt(i) == ' ') {
          counterLength++;
      }
    }
    int counterWords = 0;
    if (counterLength != str.length()) {
      for (int i = 0;i < str.length() ;i++ ) {
          if (str.charAt(i) == ' ' && str.charAt(i + 1) != ' ') {
             counterWords++;
          }
      }
    }
    System.out.println(counterWords);
  }
}
