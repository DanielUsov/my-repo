//создать строку из 100 случайных символов английского алфавита.

import java.util.Random;

class MyProgram {
    public static void main(String[] args) {
        int sizeString = 100;
        MyProgram a = new MyProgram();
        System.out.printf(a.generate(sizeString));
    }

    public String generate(int size) {
        char[] password = new char[size];
        Random random = new Random();
        String str = "";
        String lowerCase = "abcdefghijklmnopqrstuvwxyz";
        str += lowerCase;
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        str += upperCase;
        for (int i = 0; i < password.length; i++) {
            password[i] = str.charAt(random.nextInt(str.length()));
        }
        return new String(password);
    }

}
