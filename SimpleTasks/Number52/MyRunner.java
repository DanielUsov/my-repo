package Number52;

class MyRunner{
   String FIO;
   double time;

   public MyRunner(String FIO, double time){
      this.FIO = FIO;
      this.time = time;
   }

   @Override
   public String toString(){
      return this.FIO + "\n" + this.time;
   }
}
