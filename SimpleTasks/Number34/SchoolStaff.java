package Number34;
class SchoolStaff implements MyYPA{
   private String name;
   private String post;

   public SchoolStaff(String name, String post){
      this.name = name;
      this.post = post;
   }

   public String getName(){
      return this.name;
   }

   public String getPost(){
      return this.post;
   }

   public void setPost(){
      this.post = post;
   }

   public String getStaff(){
      return "Имя: " + getName() + "\n" + "Должность: " + getPost();
   }

   public String sayYPA(){
      return "YPA";
   }
}
