package Number36;

class MyStudentExcellent implements MyYPA{
   private int kolStudents;
   private String name;
   private String group;

   public MyStudentExcellent(String name, String group){
      this.name = name;
      this.group = group;
      kolStudents++;
   }

   public String getName(){
      return this.name;
   }

   public String getStudents(){
      return "Имя: " + this.name + "\n" + "Группа: " + this.group;
   }

   public String getGroup(){
      return this.group;
   }

   public void setGroup(String group){
      this.group = group;
   }

   public int getKolStudents(){
      return this.kolStudents;
   }

   public String sayYPA(){
      return "YPA";
   }
}
