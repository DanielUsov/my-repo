package task02;

import java.util.Map;

public class MapsBuilder {

    private static final int MAS_SIZE = 2000000;
    private final Map<String, Integer> myMap;

    public MapsBuilder(Map<String, Integer> myMap){
        this.myMap = myMap;
        insertMapValues();
    }

    private void insertMapValues() {
        for (int i = 0; i < MAS_SIZE; i++){
            this.myMap.put(Generators.generateString(), Generators.generateInteger());
        }
    }

    public Map<String, Integer> getMyMap() {
        return myMap;
    }

}
