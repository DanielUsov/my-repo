package task02;

import java.util.Random;

public class Generators {

    private static final int leftLimitChars = 97;
    private static final int rightLimitChars = 122;
    private static final int ARRAY_SIZE = 30000;
    private static final int STRING_LENGTH = 10;
    private static String[] randomStringArray = new String[ARRAY_SIZE];

    public static String[] generateStringArray() {
        for (int i = 0; i < randomStringArray.length; i++) {
            randomStringArray[i] = Generators.generateString();
        }
        return randomStringArray;
    }


    public static String generateString(){
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(STRING_LENGTH);
        for (int i = 0; i < STRING_LENGTH; i++) {
            int randomLimitedInt = leftLimitChars + (int)(random.nextFloat() * (rightLimitChars - leftLimitChars + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    public static Integer generateInteger(){
        return new Random().nextInt();
    }

}
