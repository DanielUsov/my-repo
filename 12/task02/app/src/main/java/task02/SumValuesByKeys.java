package task02;

public class SumValuesByKeys {

    public static int sumMapValuesFromArrayByKeys(MapsBuilder myMap, String[] array) {
        int result = 0;
        for (String i : array) {
            if (myMap.getMyMap().containsKey(i)) {
                result ++;
            }
        }
        return result;
    }
}
