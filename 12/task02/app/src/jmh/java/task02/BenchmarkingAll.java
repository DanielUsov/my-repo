package task02;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;

@BenchmarkMode(Mode.AverageTime)
public class BenchmarkingAll {

    private static MapsBuilder mapsBuilder;
    private static final MapsSorting mapsSorting = new MapsSorting();

    MapsBuilder myHashMap = new MapsBuilder(new HashMap<String, Integer>());

    @Benchmark
    public void benchmarkingBuildingHashMap() {
        mapsBuilder = new MapsBuilder(new HashMap<String, Integer>());
    }

    @Benchmark
    public void benchmarkingSortingHashMap() {
        mapsSorting.sortMap(myHashMap.getMyMap());
    }

    @Benchmark
    public void benchmarkingSumMapValuesFromArrayByKeysHashMap() {
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }

    @Benchmark
    public void benchmarkingFullHashMap() {
        mapsBuilder = new MapsBuilder(new HashMap<String, Integer>());
        mapsSorting.sortMap(myHashMap.getMyMap());
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }

    @Benchmark
    public void benchmarkingBuildingHashtable() {
        mapsBuilder = new MapsBuilder(new Hashtable<String, Integer>());
    }

    @Benchmark
    public void benchmarkingSortingHashtable() {
        mapsSorting.sortMap(myHashMap.getMyMap());
    }

    @Benchmark
    public void benchmarkingSumMapValuesFromArrayByKeysHashtable() {
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }

    @Benchmark
    public void benchmarkingFullHashtable() {
        mapsBuilder = new MapsBuilder(new Hashtable<String, Integer>());
        mapsSorting.sortMap(myHashMap.getMyMap());
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }

    @Benchmark
    public void benchmarkingBuildingLinkedLinkedHashMap() {
        mapsBuilder = new MapsBuilder(new LinkedHashMap<String, Integer>());
    }

    @Benchmark
    public void benchmarkingSortingLinkedLinkedHashMap() {
        mapsSorting.sortMap(myHashMap.getMyMap());
    }

    @Benchmark
    public void benchmarkingSumMapValuesFromArrayByKeysLinkedLinkedHashMap() {
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }

    @Benchmark
    public void benchmarkingFullLinkedLinkedHLinkedHashMap() {
        mapsBuilder = new MapsBuilder(new LinkedHashMap<String, Integer>());
        mapsSorting.sortMap(myHashMap.getMyMap());
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }

    @Benchmark
    public void benchmarkingBuildingLinkedTreeMap() {
        mapsBuilder = new MapsBuilder(new TreeMap<String, Integer>());
    }

    @Benchmark
    public void benchmarkingSortingLinkedTreeMap() {
        mapsSorting.sortMap(myHashMap.getMyMap());
    }

    @Benchmark
    public void benchmarkingSumMapValuesFromArrayByKeysTreeMap() {
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }

    @Benchmark
    public void benchmarkingFullLinkedTreeMap() {
        mapsBuilder = new MapsBuilder(new TreeMap<String, Integer>());
        mapsSorting.sortMap(myHashMap.getMyMap());
        SumValuesByKeys.sumMapValuesFromArrayByKeys(mapsBuilder,Generators.generateStringArray());
    }


}

