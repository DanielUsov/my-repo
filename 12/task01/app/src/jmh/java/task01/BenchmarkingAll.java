package task01;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Hashtable;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.Level;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class BenchmarkingAll {

    private static MapsBuilder mapsBuilder;
    private static final MapsSorting mapsSorting = new MapsSorting();

//-----
    MapsBuilder myHashMap = new MapsBuilder(new HashMap<String, Integer>());

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingBuildingHashMap() {
        mapsBuilder = new MapsBuilder(new HashMap<String, Integer>());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingSortingHashMap() {
        mapsSorting.sortMap(myHashMap.getMyMap());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingFullHashMap() {
        mapsBuilder = new MapsBuilder(new HashMap<String, Integer>());
        mapsSorting.sortMap(myHashMap.getMyMap());
    }

//-----

    MapsBuilder myHashtable = new MapsBuilder(new HashMap<String, Integer>());

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingBuildingHashtable() {
        mapsBuilder = new MapsBuilder(new Hashtable<String, Integer>());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingSortingHashtable() {
        mapsSorting.sortMap(myHashtable.getMyMap());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingFullHashtable() {
        mapsBuilder = new MapsBuilder(new Hashtable<String, Integer>());
        mapsSorting.sortMap(myHashtable.getMyMap());
    }

//-----

    MapsBuilder myLinkedHashMap = new MapsBuilder(new LinkedHashMap<String, Integer>());

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingBuildingLinkedHashMap() {
        mapsBuilder = new MapsBuilder(new LinkedHashMap<String, Integer>());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingSortingLinkedHashMap() {
        mapsSorting.sortMap(myLinkedHashMap.getMyMap());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingFullLinkedHashMap() {
        mapsBuilder = new MapsBuilder(new LinkedHashMap<String, Integer>());
        mapsSorting.sortMap(myLinkedHashMap.getMyMap());
    }

//-----

    MapsBuilder myTreeMap = new MapsBuilder(new TreeMap<String, Integer>());

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingBuildingTreeMap() {
        mapsBuilder = new MapsBuilder(new TreeMap<String, Integer>());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingSortingTreeMap() {
        mapsSorting.sortMap(myTreeMap.getMyMap());
    }

    @Setup(Level.Trial)
    @Benchmark
    public void benchmarkingFullTreeMap() {
        mapsBuilder = new MapsBuilder(new TreeMap<String, Integer>());
        mapsSorting.sortMap(myTreeMap.getMyMap());
    }

}
