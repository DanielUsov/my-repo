
/**
 * The Holder is used to data storage
 * @author DanielUsov
 * @version 1.0
 */
class Holder{

   /** @value the field stores the name of the holder */
   private String name;

   /** @value the field stores the phone of the holder */
   private String phone;

   /**
    * method getName
    * @return holder name
    */
   public String getName(){
      return this.name;
   }

   /**
    * method setName
    * @param name - holder name
    */
   public void setName(String name){
      this.name = name;
   }

   /**
    * method getPhone
    * @return phone name
    */
   public String getPhone(){
      return this.phone;
   }

   /**
    * method setPhone
    * @param phone - holder phone
    */
   public void setPhone(String phone){
      this.phone = phone;
   }

   /**
    * method toString
    * @return string representation of the holder
    */
   public String toString () {
        return "name: " + name +", phone: " + phone;
   }
}
