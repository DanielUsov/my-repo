import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

@WebServlet("/browsingDatabase")
public class Servlet extends HttpServlet {

    /**
     * method doGet
     * @param request  - an object containing, the request received from the holder
     * @param response - an object that defines the response holder
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head><title>Browsing Database</title></head>");
        out.println("<body>");
        out.println("<h1>База данных</h1>");
        DBConnection connection = new DBConnection();
        String query = "select name, phone from holder";
        try {
            Statement statement = connection.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            Holder person = new Holder();
            while (resultSet.next()){
                person.setName(resultSet.getString("name"));
                person.setPhone(resultSet.getString("phone"));
                out.println("<p>" + person + "</p>");
            }
            if (statement != null) {
               statement.close();
            }
        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        out.println("</body></html>");
        out.close();
    }
}
