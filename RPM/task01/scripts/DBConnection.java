import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The DBConnection class is used to connection to DB
 * @author DanielUsov
 * @version 1.0
 */
public class DBConnection {

    /**
     * @value username for connect to DB
     */
    private final String USERNAME = "postgres";

    /**
     * @value password for connect to DB
     */
    private final String PASSWORD = "123";

    /**
     * @value url(jdbc) for connect to DB
     */
    private final String URL = "jdbc:postgresql://127.0.0.1:5432/postgres";

    /**
     * @value connection to DB
     */
    private Connection connection;

    /**
     * DBConnection constructor
     */
    public DBConnection() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * method getConnection
     * @return connection
     */
    public Connection getConnection() {
        return connection;
    }
}
