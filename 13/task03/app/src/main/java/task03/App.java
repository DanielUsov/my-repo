package task03;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class for replacing an unloved word with a favorite
 * @author DanielUsov
 * @version 1.0
 */
public class App {

    /**
     * method replaceWordSecondV
     * @param sentence - input text
     * @param unlovedWords - search words
     * @param wordToReplace - replace word
     * @return users for the card
     */
    public static String replaceWordSecondV(String sentence, String[] unlovedWords, String wordToReplace){
        Pattern myPattern = Pattern.compile("\\b(" + String.join("|", unlovedWords) + ")\\b");
        Matcher myMatcher = myPattern.matcher(sentence);
        return myMatcher.replaceAll(wordToReplace);
    }
}
