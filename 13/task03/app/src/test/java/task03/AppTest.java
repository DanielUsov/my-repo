package task03;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void appTrue() {
        assertEquals(App.replaceWordSecondV("My name is Oleg I'm learning Java", new String[] {"Oleg"}, "Daniel"), "My name is Daniel I'm learning Java");
    }

    @Test void appFalse() {
        assertNotEquals(App.replaceWordSecondV("My name is Oleg I'm learning Java", new String[] {"Oleg"}, "Oleg"), "My name is Daniel I'm learning Java");
    }

    @Test void appTrueALotOfWords() {
        assertEquals(App.replaceWordSecondV("My name is Daniel. I want to be successful. I won't be a millionaire.", new String[] {"want to be", "won't be"}, "will be"), "My name is Daniel. I will be successful. I will be a millionaire.");
    }


}
