package task02;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void testOne() {
        App myApp = new App();
        assertEquals(myApp.getWordAfterFifth("шел пятый, мост пятый город. пятый раз"),"город раз ");
    }
    @Test void testTwo() {
        App myApp = new App();
        assertEquals(myApp.getWordAfterFifth("пятые сутки, я писал этот текст"),"сутки ");
    }

    @Test void testEnding() {
        App myApp = new App();
        assertEquals(myApp.getWordAfterFifth("пятыег сутки"),"");
    }

    @Test void testSpec() {
        App myApp = new App();
        assertEquals(myApp.getWordAfterFifth("распятый монстр"),"");
    }
}
