package task01;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void appGood() {
        App myApp = new App();
        assertTrue(myApp.checkPhoneFormat("+71231231123"));
        assertTrue(myApp.checkPhoneFormat("71231231123"));
        assertTrue(myApp.checkPhoneFormat("81231231123"));
        assertTrue(myApp.checkPhoneFormat("8(123)1231123"));
        assertTrue(myApp.checkPhoneFormat("8123123-11-23"));
        assertTrue(myApp.checkPhoneFormat("8(123)123-11-23"));
        assertTrue(myApp.checkPhoneFormat("+7(123)123-11-23"));
    }
    @Test void appNoGood() {
        App myApp = new App();
        assertFalse(myApp.checkPhoneFormat("61231231123"));
        assertFalse(myApp.checkPhoneFormat("91231231123"));
        assertFalse(myApp.checkPhoneFormat("8(1231231123"));
        assertFalse(myApp.checkPhoneFormat("812312311-23"));
        assertFalse(myApp.checkPhoneFormat("8(123123-1123"));
    }
}
