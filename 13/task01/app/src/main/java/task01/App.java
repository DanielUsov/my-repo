package task01;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Phone number verification class
 * @author DanielUsov
 * @version 1.1
 */
public class App {
   /**
    * method checkPhoneFormat
    * @param phone phone number string
    * @return Boolean value about correct phone number
    */
   public static boolean checkPhoneFormat(String phone){
      Pattern p = Pattern.compile("^(\\+7|7|8)((([(])(\\d{3})([)]))|(\\d{3}))((\\d{7})|(\\d{3})(-)(\\d{2})(-)(\\d{2}))$");
      Matcher m = p.matcher(phone);
      if (m.matches()) {
         return true;
      }
      else {
         return false;
      }
   }
}
