#include <stdio.h>
int main() {
    int a[] = {1,50,6,8,-9, 5};
    int n = 5;
    int y = 1;
    for(int x = n ; x > 0 && y > 0; x--) {
        for(int i = 0 ; i < x; i++) {
            if(a[i] > a[i+1]) {
                int m = a[i];
                a[i] = a[i+1];
                a[i+1] = m;
                y++;
            }
        }
    }
    for(int j = 0; j < n+1; ++j) {
        printf("%d ", a[j]);
    }
 }
