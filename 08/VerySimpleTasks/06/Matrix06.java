public class Matrix06{
    public static void main(String[] args) {
        int[][] mass = {{9, 7, 6}, {2, 1, 4}, {1, 0, 7}};
        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass.length; j++) {
                if (i > j) {
                    System.out.print(mass[i][j] + ",");
                }
            }
        }
        System.out.println(" ");
    }
}
