/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package SymbolsInStr;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void symbolInStr() {
      String stroka = "sadafavaarqwdqfafas";
      char symbol = 'd';
      assertEquals(2, App.symbolInStr(stroka, symbol));
    }
}
