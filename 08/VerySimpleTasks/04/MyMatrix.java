public class MyMatrix {
    public static void main(String[] args) {
        int[][] matrix = {{9, 7, 6}, {2, 1,  4}, {1,  0, 7}};
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++){
                System.out.print(matrix[j][i]);
            }
            System.out.println("\n-----");
        }
    }
}
