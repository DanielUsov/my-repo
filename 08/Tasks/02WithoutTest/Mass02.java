import java.util.Scanner;
import java.util.*;
import java.util.concurrent.TimeUnit;

class Mass02{
  public static void main(String[] args) {
    try{
      Scanner sc = new Scanner(System.in);
      System.out.print("\033[H\033[2J");
      System.out.flush();
      System.out.println("Введит количество чисел:");
      int g = 0;
      if (sc.hasNextInt()) {
        g = sc.nextInt();
      } else {
        System.out.println("Извините, но это явно не число. Перезапустите программу и попробуйте снова!");
      }
      double[] mass = new double[g];
      sc.close();
      int i = 0;
      System.out.println("--------------------------------------");
      long start = System.currentTimeMillis();
      while (i < g) {
        mass[i] = (int)(Math.random() * 100);
        i++;
      }
      Thread.sleep(1000);
      long timeWorkCode = System.currentTimeMillis() - start;
      System.out.println("\n--------------------------------------");
      double time = timeWorkCode / 1000;
      if (time > 5){
        System.out.println("превышено допустимое время работы программы, введите количество поменьше");
      }
      else if (time <= 5){
        for (int u = 0; u < mass.length;u++){
          System.out.print(mass[u] + ",");
        }
        System.out.println("\n--------------------------------------");
        System.out.println(timeWorkCode + " миллисекунд");
        System.out.println(time + "секунд");
      }
    } catch (Exception e) {
          System.out.println("Получили исключение!");
    }
  }
}
