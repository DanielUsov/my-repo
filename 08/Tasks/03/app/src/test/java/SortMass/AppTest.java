/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package SortMass;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void createSortingArray() {
      double[] a1 = {4,1,7,12,55};
      double[] a2 = {1,4,7,12,55};
      assertArrayEquals(a2,App.createSortedArray(a1));
  }
  @Test void sorting() {
      double[] a1 = {4,1,7,12,55};
      App.sort(a1);
      double[] a2 = {1,4,7,12,55};
      assertArrayEquals(a2, a1);
  }
  @Test void createSortingEmptyArray() {
      double[] a1 = {};
      double[] a2 = {};
      assertArrayEquals(a2,App.createSortedArray(a1));
  }
  @Test void sortingEmptyArray() {
      double[] a1 = {};
      App.sort(a1);
      double[] a2 = {};
      assertArrayEquals(a2, a1);
  }
  @Test void createSortingSingleArray() {
      double[] a1 = {4};
      double[] a2 = {4};
      assertArrayEquals(a2,App.createSortedArray(a1));
  }
  @Test void sortingSingleArray() {
      double[] a1 = {4};
      App.sort(a1);
      double[] a2 = {4};
      assertArrayEquals(a2, a1);
  }



}
