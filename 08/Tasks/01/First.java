import java.util.Scanner;

class First{
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
    int g = 0;
    System.out.println("Введите число");
    if (sc.hasNextInt()) {
      g = sc.nextInt();
    } else {
      System.out.println("Извините, но это явно не число. Перезапустите программу и попробуйте снова!");
    }
    sc.close();
    if (g != 0){
      System.out.println(g);
    }
  }
}
