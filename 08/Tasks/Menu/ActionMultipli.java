package Menu;

import java.util.*;

class ActionMultipli extends Action{
   String message;

   public ActionMultipli(String message){
      super(message);
   }

   public void act() throws Exception{
      Scanner sc = new Scanner(System.in);
      System.out.println("Введите первое число: ");
      long a = sc.nextInt();
      System.out.println("Введите второе число: ");
      long b = sc.nextInt();
      System.out.println("Произведение чисел равна: " + multipli(a,b));
   }

   public long multipli(long a, long b){
      return a * b;
   }
}
