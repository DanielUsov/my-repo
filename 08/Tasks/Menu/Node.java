package Menu;

import java.util.*;

class Node {
   public ArrayList<Action> action = new ArrayList<Action>();
   private Node parent;
   public ArrayList<Node> children = new ArrayList<Node>();

   public ArrayList<Action> getAction(){
      return this.action;
   }

   public void setAction(Action action){
      this.action.add(action);
   }

   public Node getParent(){
      return this.parent;
   }

   public void setParent(Node parent){
      this.parent = parent;
   }

   public ArrayList<Node> getChildren(){
      return this.children;
   }

   public void setChildren(Node children){
      this.children.add(children);
      children.parent = this;
   }

   public boolean hasParent(){
      return this.parent == null;

   }

   public int sizeChildren(){
      return this.children.size();
   }

   public int sizeAction(){
      return this.action.size();
   }

   public void output(){
      System.out.println("\nПункты меню: ");
      if (hasParent()){
         System.out.println("0: Выйти");
      }
      else{
         System.out.println("0: Вернуться на уровень назад");
      }
      int num = 1;
      for (int i = 0; i < sizeChildren(); i++) {
         System.out.println(num + ": " + "Перейти на следующий уровень");
         num++;
      }
      for (int i = 0; i < sizeAction(); i++) {
         System.out.println(num + ": " + this.action.get(i).getMessage());
         num++;
      }
   }
}
