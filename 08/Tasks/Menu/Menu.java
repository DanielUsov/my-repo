package Menu;

import java.util.*;

class Menu{
   public static void main(String[] args) throws Exception {
      Scanner sc = new Scanner(System.in);
      int userInput = 0;
      Node start = new Node();
      Node level1 = new Node();
      Node level12 = new Node();
      Action first = new ActionSum("Сложение");
      start.setChildren(level1);
      start.setChildren(level12);
      start.setAction(first);
      level1.setAction(new ActionSubtraction("Вычитание"));
      level12.setAction(new ActionMultipli("Умножение"));

      while(true){
         start.output();
         userInput = sc.nextInt();
         System.out.println();
         if (userInput == 0){
            if (start.hasParent()){
               break;
            }
            else{
               start = start.getParent();
            }
         }
         if (userInput > start.sizeChildren() && userInput <= start.sizeAction() + start.sizeChildren()){
            start.action.get(0).act();
         }
         if (userInput > 0 && userInput <= start.sizeChildren()){
            start = start.children.get(userInput - start.sizeChildren() + 1);
         }
      }
   }
}
