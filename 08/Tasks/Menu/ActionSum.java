package Menu;

import java.util.*;

class ActionSum extends Action {
   String message;

   public ActionSum(String message){
      super(message);
   }

   public void act() throws Exception{
      Scanner sc = new Scanner(System.in);
      System.out.println("Введите первое число: ");
      int a = sc.nextInt();
      System.out.println("Введите второе число: ");
      int b = sc.nextInt();
      System.out.println("Сумма чисел равна: " + sum(a,b));
   }

   public int sum(int a, int b){
      return a + b;
   }
}
