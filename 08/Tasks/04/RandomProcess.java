import java.util.*;

class RandomProcess{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите порог");
    double threshold = sc.nextDouble();
    System.out.println("Введите коэффициент влияния порога");
    double coeff = sc.nextDouble();
    System.out.println("Введите максимальное время моделирования");
    int maxTime = sc.nextInt();
    long start = System.currentTimeMillis();
    double max = coeff * threshold;
    double min = - max;
    int size = 5;
    double[][] mass = new double[size][size];
    int i = 0;
    while (maxTime > System.currentTimeMillis() - start && i < mass[0].length) {
      double number = (Math.random() * (max - min)) + min;
      System.out.println(number);
      if (number < threshold) {
        mass[0][i] = System.currentTimeMillis() - start;
        mass[1][i] = number;
        i++;
      }
    }
    for (int j = 0; j < mass[0].length; j++) {
      if (mass[1][j] != 0) {
        System.out.println("<" + mass[0][j] + ">: " + "<" + mass[1][j] + ">");
      }
    }
  }
}
